module gitlab.com/sbongini-go/ratatoskr/tests

go 1.20

require (
	github.com/phayes/freeport v0.0.0-20220201140144-74d24b5ae9f5
	github.com/rs/zerolog v1.29.0
	gitlab.alm.poste.it/go/ilog/ilog v0.1.2
	gitlab.alm.poste.it/go/ilog/zerolog v0.1.3
	gitlab.com/sbongini-go/ratatoskr/core v0.0.0-20220304094546-91866f064bae
	gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler v0.0.0-20211214133842-b73cac8e0cdb
	gitlab.com/sbongini-go/ratatoskr/source/httpsource v0.0.0-20211214114804-a5acd0cd4fac
	gitlab.com/sbongini-go/ratatoskr/source/kafkasource v0.0.0-20211214123405-560739289b69
	go.opentelemetry.io/otel/exporters/jaeger v1.3.0
	go.opentelemetry.io/otel/exporters/stdout/stdouttrace v1.3.0
	gotest.tools v2.2.0+incompatible
)

replace gitlab.com/sbongini-go/ratatoskr/core => ../core

replace gitlab.com/sbongini-go/ratatoskr/source/kafkasource => ../source/kafkasource

replace gitlab.com/sbongini-go/ratatoskr/source/httpsource => ../source/httpsource

replace gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler => ../handler/kafkahandler

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.1.2 // indirect
	github.com/confluentinc/confluent-kafka-go/v2 v2.0.2 // indirect
	github.com/felixge/httpsnoop v1.0.2 // indirect
	github.com/go-logr/logr v1.2.3 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/icza/dyno v0.0.0-20210726202311-f1bafe5d9996 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/matttproud/golang_protobuf_extensions v1.0.2-0.20181231171920-c182affec369 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/prometheus/client_golang v1.14.0 // indirect
	github.com/prometheus/client_model v0.3.0 // indirect
	github.com/prometheus/common v0.37.0 // indirect
	github.com/prometheus/procfs v0.8.0 // indirect
	gitlab.alm.poste.it/go/ilog/stdlog v0.1.2 // indirect
	gitlab.com/sbongini-go/mapstructureplus v0.0.0-20211221083647-be241237238d // indirect
	go.opentelemetry.io/contrib/instrumentation/net/http/otelhttp v0.28.0 // indirect
	go.opentelemetry.io/otel v1.12.0 // indirect
	go.opentelemetry.io/otel/internal/metric v0.26.0 // indirect
	go.opentelemetry.io/otel/metric v0.26.0 // indirect
	go.opentelemetry.io/otel/sdk v1.3.0 // indirect
	go.opentelemetry.io/otel/trace v1.12.0 // indirect
	golang.org/x/sys v0.0.0-20220520151302-bc2c85ada10a // indirect
	google.golang.org/protobuf v1.28.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
