package test

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"testing"

	"net/http"

	"github.com/phayes/freeport"
	"github.com/rs/zerolog"
	"gitlab.alm.poste.it/go/ilog/ilog"
	izerolog "gitlab.alm.poste.it/go/ilog/zerolog"
	"gitlab.com/sbongini-go/ratatoskr/core"
	"gitlab.com/sbongini-go/ratatoskr/core/handler/customhandler"
	"gitlab.com/sbongini-go/ratatoskr/core/model"
	"gitlab.com/sbongini-go/ratatoskr/handler/kafkahandler"
	"gitlab.com/sbongini-go/ratatoskr/source/httpsource"
	"gitlab.com/sbongini-go/ratatoskr/source/httpsource/route"
	"gitlab.com/sbongini-go/ratatoskr/source/kafkasource"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/exporters/stdout/stdouttrace"
	"gotest.tools/assert"
)

func startCore(httpServerPort int, kafkaBootstrapServers string, topic string, completeChan chan struct{}) core.Core {
	// Canale su cui arrivera una notifica quando il Core e' ready
	chanReady := make(chan struct{})

	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix

	//logger := stdlog.NewStdLog(stdlog.WithLevel(ilog.DEBUG))
	logger := izerolog.NewZeroLog(
		izerolog.WithLogger(zerolog.New(os.Stderr).With().Timestamp().Logger()),
		izerolog.WithLevel(ilog.DEBUG),
	)

	jaegerExporter, _ := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://127.0.0.1:14268/api/traces")))
	stdoutExporter, _ := stdouttrace.New()

	// Contatore dei messaggi processati
	var counter int = 0

	coreObj := core.NewCore(
		core.WithServiceName("ratatoskr"),
		core.WithServiceVersion("v0.1.0"),
		core.WithServiceNamespace("ns-demo"),
		core.WithServiceAttribute("environment", "demo"),
		core.WithLogger(logger),
		core.WithSpanExporter(stdoutExporter),
		core.WithSpanExporter(jaegerExporter),
		core.WithChanReady(chanReady),
		core.WithSource("http-source", httpsource.New(
			httpsource.WithPort(httpServerPort),
			httpsource.WithRoute(
				route.Builder().
					Method("POST").
					Path("/api/v1/test/:id").
					HeaderMapping("Content-Type", "Content-Type").
					Handler("kafka-producer").
					Build(),
			),
		)),
		core.WithSource("kafka-source", kafkasource.New(map[string]interface{}{
			"bootstrap.servers": kafkaBootstrapServers,
			"group.id":          "myGroup",
			"auto.offset.reset": "earliest",
		}, []string{topic},
			kafkasource.WithNextHandler("end"),
		)),
		core.WithHandler("kafka-producer", kafkahandler.New(map[string]interface{}{
			"bootstrap.servers": kafkaBootstrapServers,
		}, topic)),
		core.WithHandler("end", customhandler.New(func(ctx context.Context, bridge *core.Bridge, event model.Event) (model.Event, error) {
			fmt.Println("===================> [", counter, "]", string(event.Data))
			counter++
			completeChan <- struct{}{} // Notifico il completamento del test
			return event, nil
		})),
	)

	go coreObj.Start()

	<-chanReady // Dopo questa riga il Core sara ready

	return coreObj
}

// TestKafka unit test che:
// 1) Effettua una chimata REST
// 2) L'HTTPSource inoltra al Handler che produce il messaggio su Kafka
// 3) L'handler Kafka consuma il messaggio da kafka che lo inoltra al handler custom
func TestKafka(t *testing.T) {

	//t.Skip("Necessita di un istanza Kafka con il topic \"test\" gia creato")

	kafkaBoostrapServer := "127.0.0.1"
	topic := "test"

	// Trovo una porta casuale libera che impostero come porta del server HTTP
	httpServerPort, err := freeport.GetFreePort()
	assert.NilError(t, err)

	completeChan := make(chan struct{}, 1) // Canale su cui arrivera la notifica di completamento
	coreObj := startCore(httpServerPort, kafkaBoostrapServer, topic, completeChan)
	defer coreObj.Stop()

	// Effetuto la chiamata REST
	request, err := http.NewRequest(
		"POST",
		fmt.Sprintf("http://localhost:%d%s", httpServerPort, "/api/v1/test/1234"),
		bytes.NewBuffer([]byte(`{"name": "morpheus","job": "leader"}`)),
	)
	assert.NilError(t, err)
	request.Header.Set("Content-Type", "application/json; charset=UTF-8")

	httpClient := &http.Client{}
	resp, err := httpClient.Do(request)
	assert.NilError(t, err)
	assert.Equal(t, resp.StatusCode, http.StatusOK)

	<-completeChan
}
